FROM python:3.9-bullseye

RUN mkdir -p /opt/push-proxy/confs
WORKDIR /opt/push-proxy
RUN touch confs/push-proxy.ini

COPY requirements.txt push-proxy.py ./
RUN pip3 install -r requirements.txt

RUN useradd -u 66666 -Umd /opt/push-proxy -s /bin/false push
USER push

ENTRYPOINT [ "python3", "push-proxy.py" ]
