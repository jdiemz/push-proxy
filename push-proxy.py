#!/usr/bin/env python
"""
Push-proxy

Usage: push-proxy.py [--config FILE]

"""

import codecs
import configparser
from docopt import docopt
from http import HTTPStatus
import http.server
from http.server import BaseHTTPRequestHandler
from http.server import HTTPServer
from http.server import ThreadingHTTPServer
import json
import os
from pprint import pprint
import requests
import secrets
import signal
import sys
import time


VERSION="push-proxy v0.7"
CONFIG = {}


class HTTPProxyHandler(BaseHTTPRequestHandler):
    def handle_one_request(self):
        """Handle a single HTTP request.

        You normally don't need to override this method; see the class
        __doc__ string for information on how to handle specific HTTP
        commands such as GET and POST.

        """
        try:
            self.req_id = secrets.token_bytes(4).hex()
            self.raw_requestline = self.rfile.readline(65537)
            if len(self.raw_requestline) > 65536:
                self.requestline = ''
                self.request_version = ''
                self.command = ''
                self.send_error(HTTPStatus.REQUEST_URI_TOO_LONG)
                return
            if not self.raw_requestline:
                self.close_connection = True
                return
            if not self.parse_request():
                # An error code has been sent, just exit
                return
            mname = 'do_' + self.command
            if not hasattr(self, mname):
                self.send_error(
                    HTTPStatus.NOT_IMPLEMENTED,
                    "Unsupported method (%r)" % self.command)
                return
            method = getattr(self, mname)
            method()
            self.wfile.flush() #actually send the response if not already done.
        except socket.timeout as e:
            #a read or a write timed out.  Discard this connection
            self.log_error("Request timed out: %r", e)
            self.close_connection = True
            return

    def log_message(self, format, *args):
        """Log an arbitrary message.

        This is used by all other logging functions.  Override
        it if you have specific logging wishes.

        The first argument, FORMAT, is a format string for the
        message to be logged.  If the format string contains
        any % escapes requiring parameters, they should be
        specified as subsequent arguments (it's just like
        printf!).

        The client ip and current date/time are prefixed to
        every message.

        """

        sys.stderr.write("[%s] %s - - [%s] %s\n" %
                         (self.req_id,
                          self.address_string(),
                          self.log_date_time_string(),
                          format%args))

    def do_POST(self):
        """This is a comment describing the function of this function.
        """
        global CONFIG
        backend = CONFIG['DEFAULT']['backend']
        clean = CONFIG['DEFAULT'].getboolean('clean_notifs')

        body = self.rfile.read(int(self.headers['Content-Length']))
        body_parsed = json.loads(body)

        if CONFIG['DEFAULT'].getboolean('verbose'):
            for h in self.headers:
                self.log_message("%s: %s" % (h, self.headers[h]))
            self.log_message(body)

        # check if either send/dest user is in config
        dest_user = None
        dest_uid = body_parsed['options']['userId']
        if dest_uid in CONFIG:
            dest_user = CONFIG[dest_uid]['username']

        send_user = None
        send_uid = None
        # Test push messages do not contain a payload section.
        # And apparently some payloads do not contain a sender.
        # Motherfuckers
        if 'payload' in body_parsed['options']:
            if 'sender' in body_parsed['options']['payload']:
                send_uid = body_parsed['options']['payload']['sender']['_id']
                if send_uid in CONFIG:
                    send_user = CONFIG[send_uid]['username']

        # check if either user configed to clean notifications
        if (dest_user and CONFIG[dest_uid].getboolean('clean_notifs')) or \
           (send_user and CONFIG[send_uid].getboolean('clean_notifs')):
            clean = True

        # check if user has custom backend
        if dest_user and CONFIG[dest_uid].get('backend', None):
            backend = CONFIG[dest_uid]['backend']

        if clean:
            #del(body_parsed['options']['text'])
            body_parsed['options']['text'] = "You have a new message!"
            body = json.dumps(body_parsed)

        # Chad trollin
        if dest_user and "notif_recv_postfix" in CONFIG[dest_uid]:
            postfix = codecs.decode(CONFIG[dest_uid]['notif_recv_postfix'], 'unicode_escape')
            body_parsed['options']['text'] += " " + postfix
            body = json.dumps(body_parsed)
        elif "notif_recv_postfix" in CONFIG['DEFAULT']:
            postfix = codecs.decode(CONFIG['DEFAULT']['notif_recv_postfix'], 'unicode_escape')
            body_parsed['options']['text'] += " " + postfix
            body = json.dumps(body_parsed)

        # Actually send alert
        if backend == 'upstream':
            # parse out gcm/apn in url
            # call proper backend handler
            pass
        else:
            # forward to backend
            url = CONFIG['backends'][backend] + self.requestline.split(' ')[1]
            source_headers = {'Content-Type': self.headers['Content-Type'],
                       'Authorization': self.headers['Authorization']}

            self.log_message("Proxying to %s for user (%s -> %s) clean: %s",
                    url,
                    send_uid,
                    dest_uid,
                    clean)

            try:
                req = requests.post(url, headers=source_headers, data=body)
            except (requests.ConnectionError, requests.Timeout):
                self.log_message("Unable to connect to backend %s", url)
                self.req_id = None
                return

            self.send_response(req.status_code)
            if 'content-type' in req.headers and req.headers['content-type'].startswith('application/json'):
                self.send_header('Content-Type', req.headers['content-type'])
                self.send_header('Content-Length', req.headers['content-length'])
                self.end_headers()
                self.wfile.write(req.content)
                self.wfile.flush()
            else:
                self.end_headers()

        self.req_id = None


def run_server(server_address, server_class=ThreadingHTTPServer, handler_class=BaseHTTPRequestHandler):
    httpd = server_class(server_address, handler_class)
    httpd.serve_forever()


def log_date_time_string():
    """Return the current time formatted for logging."""
    monthname = [None,
                 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    now = time.time()
    year, month, day, hh, mm, ss, x, y, z = time.localtime(now)
    s = "%02d/%3s/%04d %02d:%02d:%02d" % (
            day, monthname[month], year, hh, mm, ss)
    return s


def log_message(msg):
    sys.stderr.write("[%s] %s\n" % (log_date_time_string(), msg))
    sys.stderr.flush()


def load_config(fname):
    global CONFIG
    CONFIG = configparser.ConfigParser()
    CONFIG.read_file(open(fname, 'r'))
    CONFIG['DEFAULT']['config_file'] = fname


def reload_config_handler(signum, frame):
    global CONFIG
    fname = CONFIG['DEFAULT']['config_file']
    old_config = CONFIG
    sys.stderr.write("[%s] Received SIGHUP, reloading config...\n" % log_date_time_string())
    try:
        load_config(fname)
        config_report()
    except (FileNotFoundError, PermissionError):
        sys.stderr.write("Cannot read config file %s.  Not updating config\n" % fname)
        CONFIG = old_config
    except configparser.ParsingError:
        sys.stderr.write("Errors parsing config file %s.  Not updating config\n" % fname)
        CONFIG = old_config
    finally:
        sys.stderr.flush()


def config_report():
    global CONFIG
    if CONFIG['DEFAULT'].getboolean('verbose'):
        log_message("Enabling verbose logging")


def main():
    global CONFIG
    config_file = './confs/push-proxy.ini'

    args = docopt(__doc__, version=VERSION)
    if args['--config']:
        config_file = args['FILE']
    try:
        load_config(config_file)
    except (FileNotFoundError, PermissionError):
        sys.stderr.write("Cannot read config file %s.  Exiting\n" % config_file)
        sys.exit(1)
    except configparser.ParsingError:
        sys.stderr.write("Errors parsing config file %s.  Exiting\n" % config_file)
        sys.exit(1)

    signal.signal(signal.SIGHUP, reload_config_handler)

    listen_ip = CONFIG['DEFAULT'].get('listen_ip', os.environ.get('LISTEN_IP', '127.0.0.1'))
    listen_port = int(CONFIG['DEFAULT'].get('listen_port', os.environ.get('LISTEN_PORT', 8869)))

    log_message("Starting up")
    config_report()

    server_address = (listen_ip, listen_port)
    run_server(server_address, handler_class=HTTPProxyHandler)


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        pass
